Decisões tomadas:

Usei a biblioteca React e vite para criar os componentes e gerenciar o estado da aplicação.
Utilizei estilos CSS modularizados com SCSS para estilizar os componentes.
Utilizei ícones fornecidos pelas bibliotecas React Icons para adicionar ícones aos botões.
Usei o conceito de prop drilling para passar a props entre os componentes.
Utilizei a biblioteca chart.js 2 para renderizar os graficos.

Criei um componente Navbar para representar a barra de navegação.
Utilizei estilos SCSS para estilizar o componente Navbar.
Utilizei ícones fornecidos pelas bibliotecas React Icons para adicionar ícones aos botões.
Exportei o componente Navbar como padrão para uso em outros componentes.
Funcionalidades:

A barra de navegação exibe botões que representam diferentes seções da aplicação.
Ao clicar em um botão, a função setPage é chamada com o nome da seção correspondente para atualizar o estado da aplicação.

Fiz um arquivo "apiFetch" que é responsável por fazer a requisição. Ele também recebe um parâmetro setLoading, que é uma função para controlar o estado de carregamento da requisição.

Também foi criado o arquivo "dataProcessing" que é responsavel por tratar todos os dados recebidos.

Instruções para executar o projeto:

Após fazer o clone do projeto, abra o terminal na pasta raiz.
Execute o comando npm install para instalar as dependências.
Execute o comando npm run dev para iniciar a aplicação.
Acesse a aplicação no navegador pelo endereço http://localhost:5173 (padrao do vite).

Dificuldades e desafios:
Fazer a parte responsiva principalmente para celulares, pois como tinham muitas informações, a tela do celular ficou pequeno para receber principalmente o grafico linechart
